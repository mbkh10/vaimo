/* public/js/scripts.js */
$( document ).ready(function() {
	//alert(location.href+"tab/tabcontent/");
	$.getJSON( location.href+"tab/tabcontent/", function( data ) {
	  $.each(data,function(key,value){
		var titl = value.Title.toString();
		$('#'+titl.toLowerCase()).html(value.Content);
	  });
	});

	
	$.getJSON( location.href+"product/productlist/", function( data ) {
		data.sort(function(a, b) {
		    return parseFloat(a.Title) - parseFloat(b.Title);
		});
		$.each( data, function(key, value){
			var ProdImgSrc = value.Image.toString();
			var ProdTitle = value.Title.toString();
			var ProdPrice = value.Price.toString();
			console.log(ProdTitle);
			$("#product_slider_321 .thumbnail img").each(function(index, element){
				var imgSrc = $(element).attr('src');
				if(key == index) {
					$(element).attr('src',ProdImgSrc)
				}
			});
			$("#product_slider_321 .caption .title").each(function(index, element){
				if(key == index) {
					$(element).html(ProdTitle);
				}
			});
			$("#product_slider_321 .caption .price").each(function(index, element){
				if(key == index) {
					$(element).html(ProdPrice);
				}
			});

		});
	});

	$('#subscribe').click(function(){
		showSubscriptionMsgDiv('#email-wait-msg');
		var emailInp = $('#email').val();
		$.post( location.href+"newsletter/subscribe/",{ email:emailInp}, function( resp ) {
			if(resp.email){
				if(resp.email.isEmpty){
					console.log(resp.email.isEmpty);
					$('#email-fail-msg em').html(resp.email.isEmpty);
				}else if(resp.email.emailAddressInvalidFormat){
					console.log(resp.email.emailAddressInvalidFormat);
					$('#email-fail-msg em').html(resp.email.emailAddressInvalidFormat);
				}
				showSubscriptionMsgDiv('#email-fail-msg');
			}else{
				console.log(resp);
				showSubscriptionMsgDiv('#email-succ-msg');
			}
		},'json');
	});
	$('#newsLetterForm').submit(function(event){
		event.preventDefault();
	});
	showSubscriptionMsgDiv('#hideAll');
});


function showSubscriptionMsgDiv(id){
	$('#email-fail-msg').hide();
	$('#email-succ-msg').hide();
	$('#email-wait-msg').hide();
	$(id).show();
}
