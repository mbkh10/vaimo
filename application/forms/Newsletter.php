<?php

class Application_Form_Newsletter extends Zend_Form
{

    public function init()
    {
		$this->setMethod('post');
		$this->setAttrib('action','index');
		$this->setAttrib('id','newsLetterForm');
		$this->addElement('text', 'email', array(
			'placeholder'	=>		'first.lastname@vaimo.com',
			'required' 		=>	 	true,
			'label'			=>		0,
			'class'			=>		'input-text input-newsletter',
			'filter' 		=> 		array('StringTrim'),
			'validators' 	=> 		array('EmailAddress')
		));

		$this->addElement('submit', 'subscribe', array(
			'class' 		=>		'btn btn_321'
		));

		$elements = $this->getElements();
        foreach($elements as $element) {
            $element->removeDecorator('DtDdWrapper')
            ->removeDecorator('HtmlTag')
            ->removeDecorator('Label');
        }


/*
		$this->setMethod('post');
		$this->setAttrib('action','index');
		$this->setAttrib('id','newsLetterForm');
		$this->addElement('text', 'email', array(
		'placeholder'   =>  'first.lastname@vaimo.com',
		'required'   =>   true,
		'filter'   =>   array('StringTrim'),
		'validators'  =>   array('EmailAddress')
		));
		$this->addElement('submit', 'subscribe');		
*/

    }

}

