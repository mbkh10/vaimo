<?php

class ProductController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function productlistAction()
    {
        // action body

       $this->_helper->viewRenderer->setNoRender();
       $this->_helper->layout()->disableLayout();
        

            $prodArr = array(
							  array(
            						Image => "images/belt.jpg",
            						Title => "BELT", 
                      				Price => "$79.00"
									),
							  array(
            						Image => "images/iphone_cover.jpg",
            						Title => "FANCY IPHONE COVER", 
                      				Price => "$78.00"
									),
							  array(
            						Image => "images/jacket.jpg",
            						Title => "Jacket", 
                      				Price => "$135.00"
									),
							  array(
            						Image => "images/new_accessories.jpg",
            						Title => "NEW ACCESSORIES", 
                      				Price => "$36.00"
									),
							  array(
            						Image => "images/sneaker.jpg",
            						Title => "Sneaker", 
                      				Price => "$22.00"
									),
							  array( 
            						Image => "images/t_shirt.jpg",
            						Title => "T-shirt", 
                      				Price => "$43.00"
							 		),
							  array(
            						Image => "images/winter_sweatshirt.jpg",
            						Title => "WINTER SWEATSHIRT",
                      				Price => "$69.00",
									),
				              array(
            						Image => "images/women_trouser.jpg",
            						Title => "WOMEN TROUSERS", 
                      				Price => "$79.00"
									)
			           );

    echo json_encode($prodArr);
    }


}



